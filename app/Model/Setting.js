'use strict'

const Lucid = use('Lucid')

class Setting extends Lucid {
    static get table(){
      return 'eztruk_settings'
    }

    static get primaryKey(){
        return 'id'
    }

    static get createTimestamp () {
      return 'createdAt'
    }

    static get updateTimestamp () {
      return 'updatedAt'
    }
}

module.exports = Setting
