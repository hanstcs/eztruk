'use strict'

const Lucid = use('Lucid')

class Outbox extends Lucid {
  static get table(){
      return 'outbox'
  }

  static get primaryKey(){
      return 'ID'
  }

  static get createTimestamp () {
    return 'InsertIntoDB'
  }

  static get updateTimestamp () {
    return 'UpdatedInDB'
  }
}

module.exports = Outbox
