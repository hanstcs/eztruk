'use strict'

const Lucid = use('Lucid')

class Truck extends Lucid {
  static get table(){
    return 'eztruk_truck'
  }

  static get primaryKey(){
    return 'truckId'
  }

  static get createTimestamp () {
    return 'createdAt'
  }

  static get updateTimestamp () {
    return 'updatedAt'
  }

  status(){
    return this.belongsTo('App/Model/StatusTruk')
  }

}

module.exports = Truck
