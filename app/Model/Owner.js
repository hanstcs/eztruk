'use strict'

const Lucid = use('Lucid')

class Owner extends Lucid {
    static get table(){
        return 'eztruk_owner'
    }

    static get primaryKey(){
        return 'ownerId'
    }

    static get createTimestamp () {
      return 'createdAt'
    }

    static get updateTimestamp () {
      return 'updatedAt'
    }
}

module.exports = Owner
