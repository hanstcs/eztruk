'use strict'

const Lucid = use('Lucid')

class TipeTarif extends Lucid {
    static get table(){
      return 'eztruk_tipe_tarif'
    }

    static get primaryKey(){
      return 'tipeTarifId'
    }

    static get createTimestamp () {
      return 'createdAt'
    }

    static get updateTimestamp () {
      return 'updatedAt'
    }
}

module.exports = TipeTarif
