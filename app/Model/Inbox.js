'use strict'

const Lucid = use('Lucid')

class Inbox extends Lucid {
    static get table(){
      return 'inbox'
    }

    static get primaryKey(){
      return 'ID'
    }

    static get createTimestamp () {
      return 'UpdatedInDB'
    }

    static get updateTimestamp () {
      return 'UpdatedInDB'
    }
}

module.exports = Inbox
