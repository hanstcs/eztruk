'use strict'

const Lucid = use('Lucid')

class Driver extends Lucid {
    static get table(){
        return 'eztruk_driver'
    }

    static get primaryKey(){
        return 'driverId'
    }

    static get createTimestamp () {
      return 'createdAt'
    }

    static get updateTimestamp () {
      return 'updatedAt'
    }
}

module.exports = Driver
