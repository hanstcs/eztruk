'use strict'

const Lucid = use('Lucid')

class TipeTruck extends Lucid {

  static get table(){
    return 'eztruk_tipe_truck'
  }

  static get primaryKey(){
    return 'tipeTruckId'
  }

  static get createTimestamp () {
    return 'createdAt'
  }

  static get updateTimestamp () {
    return 'updatedAt'
  }
}

module.exports = TipeTruck
