'use strict'

const Lucid = use('Lucid')

class Tarif extends Lucid {
    static get table(){
      return 'eztruk_tarif'
    }

    static get primaryKey(){
      return 'tarifId'
    }

    static get createTimestamp () {
      return 'createdAt'
    }

    static get updateTimestamp () {
      return 'updatedAt'
    }
}

module.exports = Tarif
