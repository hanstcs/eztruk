'use strict'

const Lucid = use('Lucid')
const Hash = use('Hash')

class User extends Lucid {

  static get table(){
    return 'eztruk_user'
  }

  static get primaryKey(){
      return 'userId'
  }

  static get createTimestamp () {
    return 'createdAt'
  }

  static get updateTimestamp () {
    return 'updatedAt'
  }

  static boot () {
    super.boot()

    /**
     * Hashing password before storing to the
     * database.
     */
    this.addHook('beforeCreate', function * (next) {
      this.password = yield Hash.make(this.password)
      yield next
    })
  }

  apiTokens () {
    return this.hasMany('App/Model/Token')
  }

}

module.exports = User
