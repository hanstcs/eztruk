'use strict'

const Lucid = use('Lucid')

class Position extends Lucid {

    static get table(){
      return 'eztruk_position'
    }

    static get primaryKey(){
      return 'positionId'
    }

    static get createTimestamp () {
      return 'createdAt'
    }

    static get updateTimestamp () {
      return 'updatedAt'
    }
}

module.exports = Position
