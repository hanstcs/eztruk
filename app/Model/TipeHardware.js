'use strict'

const Lucid = use('Lucid')

class TipeHardware extends Lucid {

    static get table(){
      return 'eztruk_tipeHardware'
    }

    static get primaryKey(){
      return 'tipeHardware_id'
    }

    static get createTimestamp () {
      return 'createdAt'
    }

    static get updateTimestamp () {
      return 'updatedAt'
    }

}

module.exports = TipeHardware
