'use strict'

const Lucid = use('Lucid')

class TruckDriver extends Lucid {

    static get table(){
      return 'eztruk_truck_driver'
    }

    static get primaryKey(){
      return 'truckDriverId'
    }

    static get createTimestamp () {
      return 'createdAt'
    }

    static get updateTimestamp () {
      return 'updatedAt'
    }
}

module.exports = TruckDriver
