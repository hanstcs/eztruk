'use strict'

const Lucid = use('Lucid')

class Hardware extends Lucid {
    static get table(){
        return 'devices'
    }

    static get primaryKey(){
        return 'id'
    }

    static get updateTimestamp () {
      return 'lastupdate'
    }
}

module.exports = Hardware
