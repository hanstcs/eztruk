'use strict'

const Lucid = use('Lucid')

class StatusTruk extends Lucid {

    static get table(){
      return 'eztruk_status_truck'
    }

    static get primaryKey(){
        return 'statusTruckId'
    }

    static get createTimestamp () {
      return 'createdAt'
    }

    static get updateTimestamp () {
      return 'updatedAt'
    }

    truck(){
      return this.hasMany('App/Model/Truck')
    }
}

module.exports = StatusTruk
