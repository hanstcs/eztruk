'use strict'

const Lucid = use('Lucid')

class Role extends Lucid {
    static get table(){
      return 'eztruk_user_role'
    }

    static get primaryKey(){
        return 'userRoleId'
    }

    static get createTimestamp () {
      return 'createdAt'
    }

    static get updateTimestamp () {
      return 'updatedAt'
    }

}

module.exports = Role
