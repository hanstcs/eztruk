'use strict'

const Setting = use('App/Model/Setting')

class SettingController {

    * index(request, response){
      const settings = yield Setting.all()
      yield response.sendView('admin.settings', {settings: settings.toJSON()})
    }

    * store(request, response){
      const settings = new Setting()

      settings.id           = this.makeid()
      settings.namaSettings = request.input('nama')
      settings.isiSettings  = request.input('settings')
      yield settings.save();

      yield response.redirect('back');
    }

    * update(request, response){
      const id       = request.input('id')
      const settings = yield Setting.find(id)

      settings.namaSettings = request.input('nama')
      settings.isiSettings  = request.input('setting')
      yield settings.save();

      yield response.redirect('back');
    }

    makeid(){
      var text = "";

      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 32; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }
}

module.exports = SettingController
