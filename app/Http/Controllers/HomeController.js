'use strict'

const Role = use('App/Model/Role')

class HomeController {

    * homePage(request, response){
      const owner       = yield Role.query().where('roleName', 'owner').first()
      const admin       = yield Role.query().where('roleName', 'admin').first()
      const adminTarif  = yield Role.query().where('roleName', 'admin_tarif').first()
      const operator    = yield Role.query().where('roleName', 'operator').first()
      const bigScreen   = yield Role.query().where('roleName', 'big_screen').first()

      const isLoggedIn = yield request.auth.check()

      if (!isLoggedIn) {
        yield response.sendView('auth.login')
      }

      const user = request.currentUser.userRoleId

      if (user == admin.userRoleId) {
        yield response.redirect('/home/admin')
      }
      if (user == adminTarif.userRoleId) {
        yield response.redirect('/home/admin-tarif')
      }
      if (user == operator.userRoleId) {
        yield response.redirect('/home/operator')
      }
      if (user == bigScreen.userRoleId) {
        yield response.redirect('/home/big-screen')
      }
      if (user == owner.userRoleId) {
        yield response.ok('Hai Owner')
      }
      yield response.json(user)
    }
}

module.exports = HomeController
