'use strict'

const TipeTarif = use('App/Model/TipeTarif')
const Tarif     = use('App/Model/Tarif')
const Database  = use('Database')

class TarifController {

    * index(request, response){
      const tarifNormal  = yield Database
                                 .table('eztruk_tipe_tarif').where('eztruk_tipe_tarif.kotaAsal', 'Normal')
                                 .innerJoin('eztruk_tarif', 'eztruk_tipe_tarif.tipeTarifId', 'eztruk_tarif.tipeTarifId').first()

      const tarifKhusus  = yield Database
                                 .table('eztruk_tipe_tarif').whereNot('kotaAsal', 'Normal').where('eztruk_tipe_tarif.deleted', 0)
                                 .innerJoin('eztruk_tarif', 'eztruk_tipe_tarif.tipeTarifId', 'eztruk_tarif.tipeTarifId')

      //  response.json(tarifNormal)

      yield response.sendView('admin_tarif.dashboard', {tarifNormal, tarifKhusus})
    }

    * update(request, response){
      const tarifId   = request.input('id')
      const tipeId    = request.input('idtipe')
      const tarif     = yield Tarif.find(tarifId)
      const tipeTarif = yield TipeTarif.find(tipeId)

      tipeTarif.kotaAsal    = request.input('kotaAsal')
      tipeTarif.kotaTujuan  = request.input('kotaTujuan')

      tarif.tarifPerKm      = request.input('biayaPerKm')
      tarif.tarifPer100Kilo = request.input('biayaPer100')
      tarif.tarifSupir      = request.input('biayaSupir')
      tarif.tarifTambahan   = request.input('biayaTambahan')
      yield tarif.save()
      yield tipeTarif.save()

      yield response.redirect('/home')
    }

    * updateNormal(request, response){
      const tarifId     = request.input('id')
      const tarif       = yield Tarif.find(tarifId)

      tarif.tarifPerKm      = request.input('biayaPerKm')
      tarif.tarifPer100Kilo = request.input('biayaPer100')
      tarif.tarifSupir      = request.input('biayaSupir')
      tarif.tarifTambahan   = request.input('biayaTambahan')

      yield tarif.save()

      yield response.redirect('/home')
    }

    * formTarif(request, response){
      const tipeTarif = yield TipeTarif.query().whereNot('kotaAsal', 'Normal').where('deleted', 0)

      yield response.sendView('admin_tarif.form.create-tarif', {tipeTarif})
    }

    * store(request, response){
      const tarif     = new Tarif()
      const tipeTarif = new TipeTarif()

      tipeTarif.tipeTarifId = this.makeid()
      tipeTarif.kotaAsal    = request.input('kotaAsal')
      tipeTarif.kotaTujuan  = request.input('kotaTujuan')

      tarif.tarifId         = this.makeid()
      tarif.tipeTarifId     = tipeTarif.tipeTarifId
      tarif.tarifPerKm      = request.input('biayaPerKm')
      tarif.tarifPer100Kilo = request.input('biayaPer100')
      tarif.tarifSupir      = request.input('biayaSupir')
      tarif.tarifTambahan   = request.input('biayaTambahan')

      yield tipeTarif.save()
      yield tarif.save()

      yield response.redirect('/home')
    }

    * delete(request, response){
      const tarifId   = request.param('id')
      const tarif     = yield Tarif.find(tarifId)
      const tipeTarif = yield TipeTarif.find(tarif.tipeTarifId)

      tarif.deleted     = 1
      tipeTarif.deleted = 1
      yield tarif.save()
      yield tipeTarif.save()

      yield response.redirect('/home')
    }

    * detailTarif(request, response){
      const tarif   = yield Tarif.find(request.param('id'))
      const tipeTarif = yield TipeTarif.find(tarif.tipeTarifId)

      yield response.sendView('admin_tarif.detail-tarif', {tarif, tipeTarif})
    }

    makeid(){
      var text = "";

      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 32; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }
}

module.exports = TarifController
