'use strict'

const Validator = use('Validator')
const Truck = use('App/Model/Truck')
const TipeTruck = use('App/Model/TipeTruck')
const StatusTruk = use('App/Model/StatusTruk')
const Hardware = use('App/Model/Hardware')
const Database = use('Database')
const Helpers = use('Helpers')
const uploadPath = 'http://localhost:3333/uploads/'

class TruckController {

    * createTruck(request, response){
      const deviceId   = []
      const isLoggedIn = yield request.auth.check()
      const ownerId    = request.param('id')

      /*-- Get hardware_id that already used by truck --*/
      const truck     = yield Database.select('hardware_id').from('eztruk_truck').where('deleted', 0)
      for (var i = 0; i < truck.length; i++) {
        deviceId.push(truck[i].hardware_id)
      }
      /////-----//////

      const tipeTruck = yield TipeTruck.query().where('deleted', 0)
      const hardware  = yield Hardware.query().whereNotIn('id', deviceId)
      const status    = yield StatusTruk.query().where('deleted', 0)

      yield response.sendView('form.create-truck', {tipe: tipeTruck, hardware: hardware, status: status, ownerId })
    }

    * index(request, response){
      const truck = yield Truck.all()
      yield response.sendView('truck.indexTruck', {truck: truck.toJSON() })
    }

    * detail(request, response){
      const deviceId   = []
      const trukId     = request.param('trukId')
      const trukDetail = yield Truck.find(trukId)
      /*-- Get hardware_id that already used by truck --*/
      const truck     = yield Database.select('hardware_id').from('eztruk_truck').where('deleted', 0)
      for (var i = 0; i < truck.length; i++) {
        deviceId.push(truck[i].hardware_id)
      }
      const hardware  = yield Hardware.query().whereNotIn('id', deviceId)
      /////-----//////
      yield response.sendView('admin.detailTruck', {truck: trukDetail.toJSON(), hardware })
    }

    * store(request, response){
      const truck     = new Truck()
      const formData  = request.only('plat', 'no_stnk', 'jenis_armada', 'hardware')
      const rules     = { plat: 'required', no_stnk: 'required', jenis_armada: 'required', hardware:'required' }

      const validation = yield Validator.validate(formData, rules)
      const picTruck   = request.file('foto', { maxSize: '2mb', allowedExtensions: ['jpg', 'jpeg', 'png'] });
      const picStnk    = request.file('stnk', { maxSize: '2mb', allowedExtensions: ['jpg', 'jpeg', 'png'] });

      if (validation.fails()) {
        yield request
            .withOnly('plat', 'no_stnk', 'jenis_armada', 'hardware')
            .andWith({ errors: validation.messages() })
            .flash()
          response.redirect('back')
          return
      }
      const fileNameTruck = 'truk' + `${new Date().getTime()}.${picTruck.extension()}`
      yield picTruck.move(Helpers.publicPath('uploads'), fileNameTruck)
      const fileNameStnk  = 'stnk' + `${new Date().getTime()}.${picTruck.extension()}`
      yield picStnk.move(Helpers.publicPath('uploads'), fileNameStnk)

      if (!picTruck.moved()) {
        response.badRequest(avatar.errors())
        return
      }

      //Inserting data
      truck.truckId       = this.makeid();
      truck.tipeTruckId   = request.input('jenis_armada')
      truck.ownerId       = request.input('ownerId')
      truck.statusTruckId = request.input('status')
      truck.platTruck     = request.input('plat')
      truck.noSTNK        = request.input('no_stnk')
      truck.photoSTNK     = fileNameStnk
      truck.photoTruck    = fileNameTruck
      truck.hardware_id   = request.input('hardware')
      yield truck.save()

      yield response.redirect('back')

    }

    * update(request, response) {
          const trukId = request.param('trukId')
          const truck = yield Truck.find(trukId)
          const formData = request.only('plat', 'nostnk', 'status', 'kategori', 'hardware')

          //////////////////////////////////////
          // if photo is not null
          const trkPict   = request.file('foto')
          const jsonTrk   = trkPict.toJSON()
          const stnkPict   = request.file('stnk')
          const jsonStnk   = stnkPict.toJSON()

          if (jsonTrk.size!=0) {
            const truk = request.file('foto', { maxSize: '2mb', allowedExtensions: ['jpg', 'png', 'jpeg', 'PNG', 'JPEG', 'JPG'] })

            const fileName = 'truk' + `${new Date().getTime()}.${truk.extension()}`
            yield truk.move(Helpers.publicPath('uploads'), fileName)

            if (!truk.moved()) {
              response.badRequest(truk.errors())
              return
            }
            truck.photoTruck = uploadPath + fileName
          }
          if (jsonStnk.size!=0) {
            const stnk = request.file('stnk', { maxSize: '2mb', allowedExtensions: ['jpg', 'png', 'jpeg', 'PNG', 'JPEG', 'JPG'] })

            const fileName = 'stnk' + `${new Date().getTime()}.${stnk.extension()}`
            yield stnk.move(Helpers.publicPath('uploads'), fileName)

            if (!stnk.moved()) {
              response.badRequest(stnk.errors())
              return
            }
            truck.photoSTNK = uploadPath + fileName
          }

          //
          ///////////////////////////////////////////

          truck.tipeTruckId = request.input('kategori')
          truck.statusTruckId = request.input('status')
          truck.platTruck = request.input('plat')
          truck.noSTNK = request.input('nostnk')
          truck.hardware_id = request.input('hardware')

          yield truck.save()
          yield response.redirect('back')
    }

    * delete(request, response) {
          const trukId = request.param('id')
          const truck = yield Truck.find(trukId)

          truck.deleted = 1;
          truck.updatedBy = request.currentUser.userId;
          yield truck.save();

          response.redirect('back');
    }

    * indexStatus(request, response){
      const data  = yield Database
                          .table('eztruk_truck')
                          .innerJoin('eztruk_status_truck', 'eztruk_truck.statusTruckId', 'eztruk_status_truck.statusTruckId')
                          .innerJoin('eztruk_tipe_truck', 'eztruk_truck.tipeTruckId', 'eztruk_tipe_truck.tipeTruckId')
      yield response.sendView('operator.table', {truck: data})
    }

    * datastatusTruck(request, response){
      const data  = yield Database
                          .table('eztruk_truck')
                          .innerJoin('eztruk_status_truck', 'eztruk_truck.statusTruckId', 'eztruk_status_truck.statusTruckId')
                          .innerJoin('eztruk_tipe_truck', 'eztruk_truck.tipeTruckId', 'eztruk_tipe_truck.tipeTruckId')

      yield response.json(data)
    }

    makeid(){
      var text = "";

      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 32; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }

}

module.exports = TruckController
