'use strict'

const TruckDriver = use('App/Model/TruckDriver')
const Driver      = use('App/Model/Driver')
const Truck       = use('App/Model/Truck')
const Database    = use('Database')

class TruckDriverController {

  * index(request, response){
    var truckId   = []
    const ownerId = request.param('ownerId')
    const truck   = yield Truck.query().where('ownerId', ownerId)
    const driver  = yield Driver.query().where('ownerId', ownerId)

    for (var i = 0; i < truck.length; i++) {
      truckId.push(truck[i].truckId)
    }

    const truckDriver = yield Database
                              .table('eztruk_truck_driver')
                              .innerJoin('eztruk_truck', 'eztruk_truck_driver.truckId', 'eztruk_truck.truckId')
                              .innerJoin('eztruk_driver', 'eztruk_truck_driver.driverId', 'eztruk_driver.driverId')
                              .whereIn('eztruk_truck_driver.truckId', truckId).where('eztruk_truck_driver.deleted', 0)

    // yield response.json(truckDriver)
    yield response.sendView('admin.form.truck-driver', {truck, driver, truckDriver, ownerId})
  }

  * store(request, response){
    const ownerId     = request.param('ownerId')

    /////////////////////////////////////////////////////////////////////////
    // Check if the truck or driver is already in table eztruk_truck_driver//
    /////////////////////////////////////////////////////////////////////////
    const trkDrv      = yield TruckDriver.query().where('deleted', 0)

    for (var i = 0; i < trkDrv.length; i++) {
    /*-- if truck or driver already in table, just update it --*/
      if (trkDrv[i].truckId == request.input('truck')) {
        const driver   = yield TruckDriver.query().where('truckId', request.input('truck')).first()
        driver.driverId = request.input('driver')
        yield driver.save()
        yield response.redirect('back');
      }else if (trkDrv[i].driverId == request.input('driver')) {
        const truck    = yield TruckDriver.query().where('driverId', request.input('driver')).first()
        truck.truckId = request.input('truck')
        yield truck.save()
        yield response.redirect('back');
      }
    }
    /*-------------------------------------------------------*/

    const truckDriver = new TruckDriver()

    truckDriver.truckDriverId = this.makeid()
    truckDriver.truckId       = request.input('truck')
    truckDriver.driverId      = request.input('driver')
    yield truckDriver.save()

    yield response.redirect('back')
  }

  * delete(request, response){
    const truckDriverId = request.param('id')
    const truckDriver   = yield TruckDriver.find(truckDriverId)

    truckDriver.deleted = 1
    yield truckDriver.save()

    yield response.redirect('back')
  }

  makeid(){
    var text = "";

    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 32; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }
}

module.exports = TruckDriverController
