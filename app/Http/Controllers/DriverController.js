'use strict'

const Validator = use('Validator')
const Helpers = use('Helpers')
const Driver = use('App/Model/Driver')
const User = use('App/Model/User')
const Role = use('App/Model/Role')
const Database = use('Database')
const Hash = use('Hash');
const uploadPath = 'http://localhost:3333/uploads/'

class DriverController {

    * index(request, response){
      const driver = yield Driver.all()
      yield response.sendView('driver.indexDriver', {driver: driver.toJSON() })

    }

    * formCreate(request, response){
      const ownerId = request.param('id')

      yield response.sendView('form.create-driver', {ownerId})
    }

    * detail(request, response){
      const driverId      = request.param('driverId')
      const detailDriver  = yield Driver.find(driverId)
      const user          = yield User.query().where('userId', detailDriver.userId).first()
      //yield response.json(user)
      yield response.sendView('admin.detailDriver', {driver: detailDriver.toJSON(), user: user });
    }

    * store(request, response){
      const driver = new Driver()
      const user   = new User()
      const role   = yield Role.query().where('roleName', 'driver').first()

      const formData  = request.only('nama', 'no_sim', 'alamat_tinggal', 'no_hp', 'no_ktp', 'username', 'email', 'password')
      const rules     = { nama: 'required', no_sim: 'required', alamat_tinggal: 'required', no_hp: 'required', no_ktp: 'required', username: 'required', email: 'required', password: 'required'}
      const picDriver = request.file('foto', { maxSize: '2mb', allowedExtensions: ['jpg', 'jpeg', 'png'] });
      const picKtp    = request.file('scan_ktp', { maxSize: '2mb', allowedExtensions: ['jpg', 'jpeg', 'png'] });

      const validation = yield Validator.validate(formData, rules)
      if (validation.fails()) {
        yield request
            .withOnly('nama', 'no_sim', 'alamat_tinggal', 'no_hp', 'no_ktp', 'username', 'email', 'password')
            .andWith({ errors: validation.messages() })
            .flash()
          response.redirect('back')
          return
      }

      const fileNameDriver = 'driver' + `${new Date().getTime()}.${picDriver.extension()}`
      yield picDriver.move(Helpers.publicPath('uploads'), fileNameDriver)
      const fileNameKtp    = 'ktpdriver' + `${new Date().getTime()}.${picKtp.extension()}`
      yield picKtp.move(Helpers.publicPath('uploads'), fileNameKtp)

      //Inserting data
      user.userId     = this.makeid();
      user.userRoleId = role.userRoleId;
      user.username   = request.input('username')
      user.email      = request.input('email')
      user.password   = request.input('password')

      driver.driverId    = this.makeid()
      driver.userId      = user.userId
      driver.ownerId     = request.input('ownerId')
      driver.namaDriver  = request.input('nama')
      driver.noSim       = request.input('no_sim')
      driver.noKTP       = request.input('no_ktp')
      driver.alamat      = request.input('alamat_tinggal')
      driver.gambarKTP   = fileNameKtp
      driver.noHP        = request.input('no_hp')
      driver.photoDriver = fileNameDriver

      yield user.save()
      yield driver.save()

      yield response.redirect('home')
    }

    * update(request, response){
      const driverData = request.only('nama', 'no_sim', 'no_ktp', 'alamat_tinggal', 'no_hp')
      const driverId   = request.input('id')
      const driver = yield Driver.find(driverId)

      const drvPict   = request.file('foto')
      const jsonDrv   = drvPict.toJSON()
      const ktpPict   = request.file('scan_ktp')
      const jsonKtp   = ktpPict.toJSON()

      //////////////////////////////////////
      // if photo is not null
      if (jsonDrv.size!=0) {
        const avatar = request.file('foto', { maxSize: '2mb', allowedExtensions: ['jpg', 'png', 'jpeg', 'PNG', 'JPEG', 'JPG'] })

        const fileName = 'driver' + `${new Date().getTime()}.${avatar.extension()}`
        yield avatar.move(Helpers.publicPath('uploads'), fileName)

        if (!avatar.moved()) {
          response.badRequest(avatar.errors())
          return
        }
        driver.photoDriver = uploadPath + fileName
      }
      if (jsonKtp.size!=0) {
        const avatar = request.file('scan_ktp', { maxSize: '2mb', allowedExtensions: ['jpg', 'png', 'jpeg', 'PNG', 'JPEG', 'JPG'] })

        const fileName = 'ktpdriver' + `${new Date().getTime()}.${avatar.extension()}`
        yield avatar.move(Helpers.publicPath('uploads'), fileName)

        if (!avatar.moved()) {
          response.badRequest(avatar.errors())
          return
        }
        driver.gambarKTP = uploadPath + fileName
      }

      //
      ///////////////////////////////////////////

      driver.namaDriver  = request.input('nama')
      driver.noSim       = request.input('no_sim')
      driver.noKTP       = request.input('no_ktp')
      driver.alamat      = request.input('alamat_tinggal')
      driver.noHP        = request.input('no_hp')
      driver.updatedBy   = request.currentUser.userId

      yield driver.save()

      yield response.redirect('back')
    }

    * updateUser(request, response){
      const userId = request.input('userId')
      const user = yield User.find(userId)

      if (request.input('password') != '') {
        user.password = yield Hash.make(request.input('password'))
      }
      user.username = request.input('username')
      user.email    = request.input('email')
      yield user.save()

      yield response.redirect('back')
    }

    * delete(request, response){
      const driverId = request.param('id');

      const driver   = yield Driver.find(driverId)

      driver.deleted    = 1
      driver.updatedBy  = request.currentUser.userId
      yield driver.save()

      yield response.redirect('back')
    }

    makeid(){
      var text = "";

      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 30; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }
}

module.exports = DriverController
