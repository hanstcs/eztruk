'use strict'

const Validator = use('Validator')
const Helpers = use('Helpers')
const Owner = use('App/Model/Owner')
const Truck = use('App/Model/Truck')
const Driver = use('App/Model/Driver')
const User = use('App/Model/User')
const Role = use('App/Model/Role')
const Hardware = use('App/Model/Hardware')
const File = use('AdonisFilesystem/Filesystem')
const Database = use('Database')
const Hash = use('Hash')
const uploadPath = 'http://localhost:3333/uploads/'

class OwnerController {

    * index (request, response){
      const owner = yield Owner.query().where('deleted', 0)
      yield response.sendView('admin.dashboard', {owner: owner })
    }

    * detailOwner(request, response){
      const deviceId   = []
      const ownerId     = request.param('ownerId')
      const ownerDetail = yield Owner.findOrFail(ownerId)
      /*-- Get hardware_id that already used by truck --*/
      const truckAll     = yield Database.select('hardware_id').from('eztruk_truck').where('deleted', 0)
      for (var i = 0; i < truckAll.length; i++) {
        deviceId.push(truckAll[i].hardware_id)
      }
      const hardware  = yield Hardware.query().whereNotIn('id', deviceId)
      /////-----//////

      const userDetail  = yield Database.from('eztruk_user').where('userId', 'LIKE', ownerDetail.userId).first()
      const truck       = yield Truck.query().where('ownerId', ownerId).where('deleted', 0)
      const driver      = yield Driver.query().where('ownerId', ownerId).where('deleted', 0)
      //yield response.json(status)
      yield response.sendView('admin.detailOwner', {detail: ownerDetail.toJSON(), user: userDetail , truck: truck, driver: driver, hardware })
    }

    * upload(request, response){
        const avatar = request.file('foto', {
          maxSize: '2mb',
          allowedExtensions: ['jpg', 'png', 'jpeg']
        })

        //response.json(avatar.toJSON())

        const userId = "L2adPXGaPfZGXBUMi2nKyf9Eipjrwb"
        const user = yield Owner.findOrFail(userId)

        const fileName = `${new Date().getTime()}.${avatar.extension()}`
        yield avatar.move(Helpers.publicPath('uploads'), fileName)

        if (!avatar.moved()) {
          response.badRequest(avatar.errors())
          return
        }

        user.photoDriver = fileName
        yield user.save()
        response.ok('Avatar updated successfully')

    }

    * store (request, response){
        const owner = new Owner()
        const user = new User()
        const role = yield Role.query().where('roleName', 'owner').first()

        const formData = request.only('nama', 'no_hp', 'no_ktp', 'no_rekening', 'bank', 'username', 'email', 'password')
        const ownerData = request.only('nama', 'no_hp', 'no_ktp', 'no_rekening', 'bank')
        const rules = { nama: 'required', no_ktp: 'required', no_rekening: 'required', bank: 'required', username: 'required', email: 'required', password: 'required' }

        //validate the data with the rules
        const validation = yield Validator.validate(formData, rules)
        const avatar = request.file('foto', {
          maxSize: '2mb',
          allowedExtensions: ['jpg', 'png', 'jpeg']
        })

        //if validation fail, back with error message
        if (validation.fails()) {
          yield request
              .withOnly('nama', 'no_hp', 'no_ktp', 'no_rekening', 'bank', 'username', 'email', 'password')
              .andWith({ errors: validation.messages() })
              .flash()

            response.redirect('back')
            return
        }

        /*--Foto--*/
        const fileName = 'owner' + `${new Date().getTime()}.${avatar.extension()}`
        yield avatar.move(Helpers.publicPath('uploads'), fileName)

        if (!avatar.moved()) {
          response.badRequest(avatar.errors())
          return
        }
        /*-------*/

        /*Inserting the data to user and owner table*/

        user.userId = this.makeid();
        user.userRoleId = role.userRoleId;
        user.username = request.input('username')
        user.email = request.input('email')
        user.password = request.input('password')

        owner.ownerId = this.makeid();
        owner.userId = user.userId;
        owner.namaOwner = request.input('nama')
        owner.noRekening = request.input('no_rekening')
        owner.bank = request.input('bank')
        owner.noHP = request.input('no_hp')
        owner.noKTP = request.input('no_ktp')
        owner.photoOwner = uploadPath + fileName

        yield user.save()
        yield owner.save()

        yield response.redirect('/home')

    }

    * edit(request, response) {
          const id = request.param('id')
          const owner = yield Owner.find(id)
          const account = yield User.find(owner.userId)

          yield response.sendView('form.edit-owner', {owner: owner, account: account})
    }

    * update(request, response) {
          var ownerData = request.only('ownerId', 'namaOwner', 'noRekening', 'bank', 'noHP', 'noKTP');
          var checkpass = request.only('password');
          const ownerid = ownerData.ownerId;
          const owner = yield Owner.find(ownerid);

          const pict = request.file('foto')
          const jsonPict = pict.toJSON()

          ///////////////////////
          // if photo is not null
          ///////////////////////
          if (jsonPict.size!=0) {
              const avatar = request.file('foto', {
                maxSize: '2mb',
                allowedExtensions: ['jpg', 'png', 'jpeg']
              })
              /*--Foto--*/
              const fileName = 'owner' + `${new Date().getTime()}.${avatar.extension()}`
              yield avatar.move(Helpers.publicPath('uploads'), fileName)

              if (!avatar.moved()) {
                response.badRequest(avatar.errors())
                return
              }
              owner.photoOwner = uploadPath + fileName
          }
          ///////////////////////

          if (checkpass.password == ''){
              var accountData = request.only('username', 'email');

              const accountid = owner.userId;
              const account = yield User.find(accountid);

              account.fill(accountData);
              yield account.save();

          } else if (checkpass.password != ''){
              var accountData = request.only('username', 'email', 'password')
              accountData.password = yield Hash.make(accountData.password)

              const accountid = owner.userId;
              const account = yield User.find(accountid);

              account.fill(accountData);
              yield account.save();
          }

          // Update and save post
          //owner.photoOwner = fileName;
          owner.fill(ownerData);
          yield owner.save();

          // Go home
          response.redirect('/detail/' + ownerid);
    }

    * delete(request, response) {
          const owner = yield Owner.find(request.param('id'))
          const account = yield User.find(owner.userId)

          owner.deleted = 1;
          owner.updatedBy = request.currentUser.userId;
          yield owner.save();

          account.deleted = 1;
          owner.updatedBy = request.currentUser.userId;
          yield account.save();

          response.redirect('/daftar-owner');
    }

    makeid(){
      var text = "";

      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 30; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }


}

module.exports = OwnerController
