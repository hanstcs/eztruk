'use strict'

const TipeHardware = use('App/Model/TipeHardware')
const Validator = use('Validator')

class TipeHardwareController {

  * store(request, response){
    const tipeHardware = new TipeHardware()

    const formData = request.only('nama', 'format_setup', 'format_output')
    const rules = { nama: 'required', format_setup: 'required', format_output: 'required'}

    const validation = yield Validator.validate(formData, rules)
    //if validation fail, back with error message
    if (validation.fails()) {
      yield request
          .withOnly('nama','format_setup', 'format_output')
          .andWith({ errors: validation.messages() })
          .flash()

        response.redirect('back')
        return
    }

    //Inserting data
    tipeHardware.tipeHardware_id = this.makeid()
    tipeHardware.nama_tipehardware = request.input('nama')
    tipeHardware.formatSetup = request.input('format_setup')
    tipeHardware.formatGetLokasi = request.input('format_output')
    yield tipeHardware.save()

    yield response.redirect('/home')

  }

  makeid(){
    var text = "";

    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 30; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

}

module.exports = TipeHardwareController
