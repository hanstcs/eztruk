'use strict'

const Hardware = use('App/Model/Hardware')
const TipeHardware = use('App/Model/TipeHardware')
const Validator = use('Validator')

class HardwareController {

  * createForm(request, response){
      const tipeHardware = yield TipeHardware.all()
      yield response.sendView('hardware.create-hardware', {tipe: tipeHardware.toJSON() })
  }

  * index(request, response){
      const hardware = yield Hardware.query().where('deleted', 0)
      yield response.sendView('hardware.daftar-hardware', {hardware: hardware })
  }

  * store(request, response){
      const hardware = new Hardware()

      const formData = request.only('nama_hardware', 'tipe_hardware', 'phone_number')
      const rules = { nama_hardware: 'required', tipe_hardware: 'required', phone_number: 'required' }
      const validation = yield Validator.validate(formData, rules)

      if (validation.fails()) {
        yield request
            .withOnly('nama_hardware', 'tipe_hardware', 'phone_number')
            .andWith({ errors: validation.messages() })
            .flash()
          response.redirect('back')
          return
      }

      //Inserting data
      hardware.hardware_id = this.makeid()
      hardware.nama_hardware = request.input('nama_hardware')
      hardware.hardware_type = request.input('tipe_hardware')
      hardware.hardware_phonenumber = request.input('phone_number')
      hardware.status_setup = 1
      yield hardware.save()

      yield response.redirect('home')
  }

  * update(request, response){
    const harwareId = request.input('id')
    const hardware  = yield Hardware.find(harwareId)

    hardware.name     = request.input('name')
    hardware.uniqueid = request.input('uniqueid')
    hardware.phone    = request.input('phone')

    yield hardware.save()

    yield response.redirect('back')
  }

  * delete(request, response){
    const hardwareId = request.param('id');
    const hardware  = yield Hardware.find(hardwareId)

    hardware.deleted = 1
    yield hardware.save()

    yield response.redirect('back')
  }

  makeid(){
    var text = "";

    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 32; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

}

module.exports = HardwareController
