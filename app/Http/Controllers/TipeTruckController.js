'use strict'

const TipeTruck = use('App/Model/TipeTruck')

class TipeTruckController {

  * store(request, response){
    const tipeTruck = new TipeTruck()

    //inserting data
    tipeTruck.tipeTruckId = this.makeid()
    tipeTruck.namaKategori = request.input('nama_kategori')
    tipeTruck.panjangMuatan = request.input('panjang_muatan')
    tipeTruck.lebarMuatan = request.input('lebar_muatan')
    tipeTruck.tinggiMuatan = request.input('tinggi_muatan')
    tipeTruck.beratMax = request.input('berat_max')
    yield tipeTruck.save()

    yield response.redirect('back')
  }

  makeid(){
    var text = "";

    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 32; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }
}

module.exports = TipeTruckController
