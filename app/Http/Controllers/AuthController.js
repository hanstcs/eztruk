'use strict'

const User = use('App/Model/User')
const Hash = use('Hash')

class AuthController {

    * login(request, response){
        const email = request.input('email')
        const password = request.input('password')

        try {
          const authCheck = yield request.auth.attempt(email, password)

          if (authCheck) {
            return response.redirect('/home')
          }
        } catch (e) {
          yield request.with({error: e.message}).flash()
          response.redirect('/signIn')
        }

    }

    * logout(request, response){
        yield request.auth.logout()

        return response.redirect('/home')
    }

    * register(request, response){
      const user = new User()
      user.userId = this.makeid();
      user.username = request.input('username')
      user.email = request.input('email')
      user.password = request.input('password') //Password tidak perlu di hash di controller, karena di hash di model

      yield user.save()

      var registerMessage = {
          success: 'Register sukses, sekarang singkirkan kebusukanmu dari sini dan silakan login'
      }

      yield response.sendView('auth.login', {registerMessage : registerMessage })
    }

    makeid(){
      var text = "";

      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 30; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }
}

module.exports = AuthController
