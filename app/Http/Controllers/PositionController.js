'use strict'

const Position = use('App/Model/Position')
const Database = use('Database')

class PositionController {

  * getAllLocation(request, response){
    //const position = yield Position.all()
    const data  = yield Database
                        .table('eztruk_position')
                        .innerJoin('eztruk_truck', 'eztruk_position.truckId', 'eztruk_truck.truckId')
                        .innerJoin('eztruk_status_truck', 'eztruk_truck.statusTruckId', 'eztruk_status_truck.statusTruckId')
                        .innerJoin('eztruk_tipe_truck', 'eztruk_truck.tipeTruckId', 'eztruk_tipe_truck.tipeTruckId')
    yield response.json(data)
  }

  * getTruckPosition(request,response){
    const id = yield Database.max('id as id').from('positions').groupBy('deviceid')
    const newid = []
    for (var i = 0; i < id.length; i++) {
        newid.push(id[i].id)
    }

    const pemesananId = yield Database.select('pemesananId').from('eztruk_pemesanan').groupBy('truckId')
    const newpemesananId = []
    for (var i = 0; i < pemesananId.length; i++) {
        newpemesananId.push(pemesananId[i].pemesananId)
    }

    const lokasitruk = yield Database
                         .select('eztruk_truck.truckId','eztruk_pemesanan.pemesananId','platTruck', 'namaDriver', 'hardware_id', 'latitude', 'longitude',
                                 'photoTruck', 'statusName', 'namaKategori', 'panjangMuatan',
                                 'lebarMuatan', 'tinggiMuatan', 'beratMax', 'latPickup', 'longPickup', 'latDrop', 'longDrop')
                         .from('positions')
                         .innerJoin('eztruk_truck', 'deviceid', 'eztruk_truck.hardware_id')
                         .innerJoin('eztruk_status_truck', 'eztruk_truck.statusTruckId', 'eztruk_status_truck.statusTruckId')
                         .innerJoin('eztruk_tipe_truck', 'eztruk_truck.tipeTruckId', 'eztruk_tipe_truck.tipeTruckId')
                         .leftJoin('eztruk_truck_driver', 'eztruk_truck.truckId', 'eztruk_truck_driver.truckId')
                         .leftJoin('eztruk_driver', 'eztruk_truck_driver.driverId', 'eztruk_driver.driverId')
                         .leftOuterJoin('eztruk_pemesanan', 'eztruk_truck.truckId', 'eztruk_pemesanan.truckId')
                         .leftJoin('eztruk_status_pemesanan', 'eztruk_pemesanan.statusPemesananId', 'eztruk_status_pemesanan.statusPemesananId')
                         .whereIn('id', newid)
                         .andWhere('namaStatus', 'Dalam Perjalanan')
                         .orWhereNull('pemesananId')
                         .groupBy('eztruk_truck.truckId')

    yield response.json(lokasitruk)
  }

  * operatorPage(request, response){
    const position = yield Position.all()
    yield response.sendView('operator.map', { position: position.toJSON() })
  }

}

module.exports = PositionController
