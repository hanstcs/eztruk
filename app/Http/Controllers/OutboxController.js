'use strict'

const Outbox = use('App/Model/Outbox')
const Validator = use('Validator')

class OutboxController {

  * store(request, response){
    const outbox = new Outbox()
    const formData = request.only('phone_number', 'message')

    //validation
    const rules = { phone_number: 'required', message: 'required' }
    const validation = yield Validator.validate(formData, rules)

    if (validation.fails()) {
      yield request
          .withOnly('phone_number', 'message')
          .andWith({ errors: validation.messages() })
          .flash()
        response.redirect('back')
        return
    }

    outbox.DestinationNumber = request.input('phone_number')
    outbox.TextDecoded = request.input('message')
    outbox.CreatorID = "Program"
    outbox.Coding = "Default_No_Compression"
    yield outbox.save()

    yield response.redirect('home')
  }

}

module.exports = OutboxController
