'use strict'

const Inbox     = use('App/Model/Inbox')
const Database  = use('Database')
const Device    = use('App/Model/Hardware')
const Truck     = use('App/Model/Truck')
const StatusTruk = use('App/Model/StatusTruk')

class InboxController {

    * index(request, response){
      const sosDevices   = []
      const senderNumber = []
      const inbox        = yield Inbox.query().where('isRead', 0).fetch()
      const sosStatus    = yield StatusTruk.query().where('statusName', 'sos').first()
      const inboxes      = inbox.toJSON()
      /////////////////////////////////////////////
      // Define the SOS message from inbox table //
      /////////////////////////////////////////////
      for (var i = 0; i < inboxes.length; i++) {
        if (inboxes[i].TextDecoded.slice(0, 4) == "SOS,") {
          sosDevices.push(inboxes[i])
          /* Update isRead column to 1 */
          var inboxRead = yield Inbox.find(inboxes[i].ID)
          inboxRead.isRead = 1
          yield inboxRead.save()
          ///////////////////////////////
        }
        if (inboxes[i].TextDecoded.slice(0, 14) == "Emergency call") {
          sosDevices.push(inboxes[i])
          /* Update isRead column to 1 */
          var inboxRead = yield Inbox.find(inboxes[i].ID)
          inboxRead.isRead = 1
          yield inboxRead.save()
          ///////////////////////////////
        }
      }

      ///////////////////////////////////////////////
      //Push the phone number to senderNumber array//
      ///////////////////////////////////////////////
      for (var i = 0; i < sosDevices.length; i++) {
        senderNumber.push(sosDevices[i].SenderNumber)
      }
      //////////////////////////////////////////////////
      // Get data truk and device where device in SOS //
      //////////////////////////////////////////////////
      const truckSOS = yield Database
                          .table('devices')
                          .innerJoin('eztruk_truck', 'devices.id', 'eztruk_truck.hardware_id')
                          .whereIn('phone', senderNumber)
      var truck = 0;
      for (var i = 0; i < truckSOS.length; i++) {
        var truckId         = truckSOS[i].truckId
        truck               = yield Truck.find(truckId)
        truck.statusTruckId = sosStatus.statusTruckId
        yield truck.save()
      }
      //sosDevices.push.apply(sosDevices, inboxes)
      //const dev = yield Device.query().whereIn('id', [1, 2, 5])
      //var a = request.hostname()   //get hostname

      yield response.json(truck)
    }
}

module.exports = InboxController
