'use strict'

const Database = use('Database')

class TestController {

  * test(request, response){
    const id = yield Database.max('id as id').from('positions').groupBy('deviceid')
    const newid = []
    for (var i = 0; i < id.length; i++) {
        newid.push(id[i].id)
    }

    const status   = yield Database.table('eztruk_status_pemesanan').where('namaStatus', 'Dalam Perjalanan').first()
    var statusId = status.statusPemesananId

    const pemesanan = yield Database
                            .select('*')
                            .from('eztruk_pemesanan')
                            .innerJoin('eztruk_status_pemesanan', 'eztruk_pemesanan.statusPemesananId', 'eztruk_status_pemesanan.statusPemesananId')
                            .where('eztruk_pemesanan.statusPemesananId', statusId)

    const lokasi = yield Database
                         .select('*')
                         .from('positions').whereIn('id', newid)
                         .innerJoin('eztruk_truck', 'deviceid', 'eztruk_truck.hardware_id')
                         .innerJoin('eztruk_status_truck', 'eztruk_truck.statusTruckId', 'eztruk_status_truck.statusTruckId')
                         .rightOuterJoin('eztruk_tipe_truck', 'eztruk_truck.tipeTruckId', 'eztruk_tipe_truck.tipeTruckId')
                         .leftOuterJoin('eztruk_pemesanan', 'eztruk_truck.truckId', 'eztruk_pemesanan.truckId')//.where('statusPemesananId', 'LLQtxqw9AnmN2QbosxlnWGFJ50xWZI')

    yield response.json(pemesanan)
  }
}

module.exports = TestController
