'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.get('/', 'HomeController.homePage')

Route.get('/home', 'HomeController.homePage')

//////////////////////////////
//  Route for Admin Data
//////////////////////////////
Route.get('/home/admin', 'OwnerController.index').middleware('admin')

Route.get('/home/admin/detail/:ownerId/truck-driver', 'TruckDriverController.index').middleware('admin')

Route.post('/home/admin/detail/:ownerId/truck-driver/store', 'TruckDriverController.store').middleware('admin')

Route.get('/home/admin/detail/:ownerId/truck-driver/delete/:id', 'TruckDriverController.delete').middleware('admin')

Route.post('/detail-driver/update-driver', 'DriverController.update').middleware('admin')

Route.post('/detail-driver/:id/update-driver', 'DriverController.update').middleware('admin')

Route.post('/detail-driver/:id/update-driver/account', 'DriverController.updateUser').middleware('admin')

///////////////////////////////

///////////////////////////////
//  Route for Admin Tarif
///////////////////////////////
Route.get('/home/admin-tarif', 'TarifController.index').middleware('adminTarif')

Route.post('/home/admin-tarif/update', 'TarifController.update').middleware('adminTarif')

Route.post('/home/admin-tarif/update-normal', 'TarifController.updateNormal').middleware('adminTarif')

Route.get('/home/admin-tarif/create-tarif', 'TarifController.formTarif').middleware('adminTarif')

Route.post('/home/admin-tarif/create-tarif', 'TarifController.store').middleware('adminTarif')

Route.get('/home/admin-tarif/delete/:id', 'TarifController.delete').middleware('adminTarif')

Route.get('/home/admin-tarif/detail/:id', 'TarifController.detailTarif').middleware('adminTarif')

///////////////////////////////

Route.get('/home/operator').render('operator.dashboard').middleware('operator')

Route.get('/home/big-screen').render('big_screen.dashboard').middleware('bigScreen')

Route.get('/signIn').render('auth.login')

Route.post('/signIn', 'AuthController.login')

Route.get('/signOut', 'AuthController.logout')

Route.get('/register').render('auth.register')

Route.post('/register', 'AuthController.register')

Route.get('/detail/:id/create-truck', 'TruckController.createTruck').middleware('admin')

Route.post('/create-truck', 'TruckController.store').middleware('admin')

Route.post('/create-tipe-truk', 'TipeTruckController.store').middleware('admin')

Route.get('/daftar-truck', 'TruckController.index').middleware('admin')

Route.get('/detail-truck/:trukId', 'TruckController.detail').middleware('admin')

Route.post('/detail-truck/:trukId/update-truck', 'TruckController.update').middleware('admin')

Route.get('/delete/truck/:id', 'TruckController.delete').middleware('admin')

Route.on('create-owner').render('form.create-owner').middleware('admin')

Route.get('/edit-owner/:id', 'OwnerController.edit').middleware('admin')

Route.post('/update-owner', 'OwnerController.update').middleware('admin')

Route.get('/delete/:id', 'OwnerController.delete').middleware('admin')

Route.get('/daftar-driver', 'DriverController.index').middleware('admin')

Route.get('/detail/:id/create-driver', 'DriverController.formCreate').middleware('admin')

Route.post('/store-driver', 'DriverController.store').middleware('admin')

Route.get('/detail-driver/:driverId', 'DriverController.detail').middleware('admin')

Route.get('/detail-driver/:id/delete-driver', 'DriverController.delete').middleware('admin')

Route.post('store-owner', 'OwnerController.store').middleware('admin')

Route.post('/upload', 'OwnerController.upload')

Route.get('/daftar-owner', 'OwnerController.index').middleware('admin')

Route.get('/detail/:ownerId', 'OwnerController.detailOwner').middleware('admin')

Route.get('create-tipe-hardware').render('hardware.create-tipe-hardware').middleware('admin')

Route.post('create-tipe-hardware', 'TipeHardwareController.store').middleware('admin')

Route.get('create-hardware', 'HardwareController.createForm').middleware('admin')

Route.post('create-hardware', 'HardwareController.store').middleware('admin')

Route.get('/outbox').render('form.create-outbox')

Route.post('/create-outbox', 'OutboxController.store')

//
Route.get('/getAllLocation', 'PositionController.getAllLocation')

Route.get('/getAllTruckPosition', 'PositionController.getTruckPosition')
//
Route.get('operator', 'PositionController.operatorPage')

Route.get('/home/admin/devices', 'HardwareController.index').middleware('admin')

Route.post('/devices/update-devices', 'HardwareController.update').middleware('admin')

Route.get('/devices/:id/delete-devices', 'HardwareController.delete').middleware('admin')

////////////////////
// Admin Setting  //
////////////////////

Route.get('/home/admin/setting', 'SettingController.index')

Route.post('/store/settings', 'SettingController.store')

Route.post('/update/setting', 'SettingController.update')

////////////////////

Route.get('/test-2', 'TruckController.indexStatus')

Route.get('/test', 'TestController.test')

Route.get('/getSOS', 'InboxController.index')

Route.get('/getAllTruck', 'TruckController.datastatusTruck')
