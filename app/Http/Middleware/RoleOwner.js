'use strict'

const Role = use('App/Model/Role')

class RoleOwner {

  /* This is the owner role middleware
   * for defining the role before access the controller
  */
  * handle (request, response, next) {
    // here goes your middleware logic
    const owner = yield Role.query().where('roleName', 'owner').first()
    const isLoggedIn = yield request.auth.check()

    if (!isLoggedIn) {
      yield response.sendView('auth.login')
    }

    const user = request.currentUser.userRoleId

    if (user != owner.userRoleId) {
      yield response.sendView('errors.401')
    }

    // yield next to pass the request to next middleware or controller
    yield next
  }

}

module.exports = RoleOwner
