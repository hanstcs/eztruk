'use strict'

const Role = use('App/Model/Role')

class BigScreen {

  * handle (request, response, next) {
    // here goes your middleware logic
    const bigScreen = yield Role.query().where('roleName', 'big_screen').first()
    const isLoggedIn = yield request.auth.check()

    if (!isLoggedIn) {
      yield response.sendView('auth.login')
    }

    const user = request.currentUser.userRoleId

    if (user != bigScreen.userRoleId) {
      yield response.sendView('errors.401')
    }
    // yield next to pass the request to next middleware or controller
    yield next
  }

}

module.exports = BigScreen
