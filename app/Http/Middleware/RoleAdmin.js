'use strict'
const User = use('App/Model/User')
const Role = use('App/Model/Role')

class RoleAdmin {
/* This is the admin role middleware
 * for defining the role before access the controller
*/

  * handle (request, response, next) {
    // here goes your middleware logic
    const admin = yield Role.query().where('roleName', 'admin').first()
    const isLoggedIn = yield request.auth.check()

    if (!isLoggedIn) {
      yield response.sendView('auth.login')
    }

    const user = request.currentUser.userRoleId

    if (user != admin.userRoleId) {
      yield response.sendView('errors.401')
    }

    // yield next to pass the request to next middleware or controller
    yield next
  }

}

module.exports = RoleAdmin
