'use strict'

const Role = use('App/Model/Role')

class RoleOperator {
  /* This is the operator role middleware
   * for defining the role before access the controller
  */
  * handle (request, response, next) {
    // here goes your middleware logic
    const operator = yield Role.query().where('roleName', 'operator').first()
    const isLoggedIn = yield request.auth.check()

    if (!isLoggedIn) {
      yield response.sendView('auth.login')
    }

    const user = request.currentUser.userRoleId

    if (user != operator.userRoleId) {
      yield response.sendView('errors.401')
    }

    // yield next to pass the request to next middleware or controller
    yield next
  }

}

module.exports = RoleOperator
