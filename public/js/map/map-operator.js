var map;
var centerMap = { lat: 2.3508637, lng: 99.11552434 }
var markerData = [];
var newMarkers = [];
var icon = '/assets/map/truck.png';

function initMap() {
  //create map
  map = new google.maps.Map(document.getElementById('map'), {
    center: centerMap,
    zoom: 8
  });
  /*
  **  The tooltip , the ajax, the delete marker, re assign marker
  **  Markers Clusters
  **  Zoom the marker
  */
  var markers = [
    {% for locations in position %}
      {
        latitude: {{ locations.latitude}},
        longitude: {{ locations.longitude}}
      },
    {% endfor %}
  ];

  console.log(markers);

  //for map control the full screen buton on top right
  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(
    FullScreenControl(map));

  infoWindow = new google.maps.InfoWindow();

  google.maps.event.addListener(map, 'click', function() {
     infoWindow.close();
  });

  displayMarkers(markers);
}

function displayMarkers(markers) {
  for (var i = 0; i < markers.length; i++) {
    createMarker(markers[i]);
  }
}

function deleteMarker() {
  for (var i = 0; i < markerData.length; i++) {
    markerData[i].setMap(null);
  }
  markerData.length = 0;
}

function createMarker(ObjMarker) {
  //create marker
  var marker = new google.maps.Marker({
    map: map,
    title: ObjMarker.title,
    icon: icon,
    position: new google.maps.LatLng(ObjMarker.latitude, ObjMarker.longitude)
  });
  markerData.push(marker);
  //console.log(markerData);
  //tooltip
  google.maps.event.addListener(marker, 'click', function() {

     // Creating the content to be inserted in the infowindow
     var iwContent = '<div id="iw_container">' +
           '<div class="iw_title">' + "name" + '</div>' +
        '<div class="iw_content">' + "address1" + '</div></div>';

     infoWindow.setContent(iwContent);
     infoWindow.open(map, marker);
  });
}
